### Инструкция (Ubuntu)

1. ставим microk8s
1. Активируем аддоны: dns, storage, ingress

Чтобы с локального компа вызывать kubectl:

```bash
~/.kube/config > ~/.kube/config_bckp
microk8s config > ~/.kube/config
```

План развития чартов:
1. spark
1. cassandra
1. elk
1. jenkins
1. airflow
1. flink
1. storm
1. Cask™ Data Application Platform (CDAP)
1. clickhouse